package de.aixigo.codingDojo;

/*
Develop a function that recognizes whether a number is "happy" or not.

https://en.wikipedia.org/wiki/Happy_number

A Happy Number is a number in which the sum of the squares of its digits "over time" is 1. Example:

19 -> 1^2 + 9^2 = 82 -> 8^2 + 2^2 = 68 -> 6^2 + 8^2 = 100 -> 1^2 + 0^2 + 0^2 = 1
*/

public class HappyNumbers
{
}
