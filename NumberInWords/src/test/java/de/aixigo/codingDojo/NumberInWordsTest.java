package de.aixigo.codingDojo;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class NumberInWordsTest {

   @ParameterizedTest(name = "run #{index} with [{arguments}]")
   @MethodSource("generateInvalidNumber")
   void testThatExceptionIsThrownForInvalidNumbers(final int number)
   {
      IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> NumberInWords.convert(number));
      assertEquals(illegalArgumentException.getMessage(), String.valueOf(number));

   }

   @ParameterizedTest(name = "run #{index} with [{arguments}]")
   @MethodSource("generateValidNumber")
   void testThatValidNumberAreConverted(final int number, final String expected)
   {
      assertEquals(expected, NumberInWords.convert(number));
   }

   private static Stream<Arguments> generateValidNumber()
   {
      return Stream.of( new Arguments[]{
         Arguments.of(0, "zero"),
         Arguments.of(1, "one"),
         Arguments.of(2, "two"),
         Arguments.of(3, "three"),
         Arguments.of(4, "four"),
         Arguments.of(5, "five"),
         Arguments.of(6, "six"),
         Arguments.of(7, "seven"),
         Arguments.of(8, "eight"),
         Arguments.of(9, "nine"),
         Arguments.of(20, "twenty"),
         Arguments.of(21, "twenty one"),
         Arguments.of(30, "thirty"),
         Arguments.of(45, "forty five"),
         Arguments.of(100, "one hundred"),
         Arguments.of(101, "one hundred and one")
      } );
   }

   private static Stream<Arguments> generateInvalidNumber()
   {
      return Stream.of( new Arguments[]{
         Arguments.of(-1),
         Arguments.of(1000)
      } );
   }


}
