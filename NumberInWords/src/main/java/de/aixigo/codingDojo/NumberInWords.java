package de.aixigo.codingDojo;

/*
* 745.00 \$ (amount in numbers)

seven hundred and fourty five dollars (amount in words)

Step 1
The Kata is now to write a little converter class or function to convert numbers into words.

Step 2
Convert it back.

Step 3
Do all of it test driven.

*/

public class NumberInWords {
   public static String convert(final int number) throws IllegalArgumentException {

      if(number< 20) {
         return convertNumberLessThanTwenty(number);
      }
      else if( number < 100){
         return convertNumberLessThanHundred(number);
      }
      else
      {
         return convertNumberLessThanThousand(number);
      }

   }

   private static String convertNumberLessThanThousand(int number) {
      String result = convertNumberLessThanTwenty(number / 100 ) + " hundred";
      if(number % 100 != 0) {
         result += " and " + convertNumberLessThanHundred(number % 100);
      }
      return result;
   }

   private static String convertNumberLessThanHundred(int number) {
      if(number< 20) {
         return convertNumberLessThanTwenty(number);
      }
      String result;
      switch ( number / 10 ) {
         case 2:
            result = "twenty";
            break;
         case 3:
            result = "thirty";
            break;
         case 4:
            result = "forty";
            break;
         default:
            throw new IllegalArgumentException(String.valueOf(number));
      }

      if( number % 10 != 0 ) {
         result = new StringBuilder(result).append(" ").append(convertNumberLessThanTwenty(number % 10)).toString();
      }

      return result;
   }


   private static String convertNumberLessThanTwenty(int i) {
      switch (i){
         case 0: return "zero";
         case 1: return "one";
         case 2: return "two";
         case 3: return "three";
         case 4: return "four";
         case 5: return "five";
         case 6: return "six";
         case 7: return "seven";
         case 8: return "eight";
         case 9: return "nine";
         case 10: return "ten";
         case 11: return "eleven";
         case 12: return "twelve";

      }
      throw  new IllegalArgumentException(String.valueOf(i));
   }
}
