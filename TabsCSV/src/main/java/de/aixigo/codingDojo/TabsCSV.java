package de.aixigo.codingDojo;

/*
Write a function that tabs CSV lines.

   Set<String>  Tabelliere(Set<String> CSV_zeilen);
   The function receives an enumeration of strings as input. Each of these strings is a CSV line that could come from a file, for example.

   Example of a possible input:

   Name;Strasse;Ort;Alter
   Peter Pan;Am Hang 5;12345 Einsam;42
   Maria Schmitz;Kölner Straße 45;50123 Köln;43
   Paul Meier;Münchener Weg 1;87654 München;65

   In the input data, a semicolon separates the individual values ​​within the rows.
   More complicated CSV mechanisms (such as a semicolon in the data) do not need to be considered.
   The input data is always structured correctly, no error handling is required.

   As output, the function returns a formatted table of the input data. The first line of the input data is used as the headline.
   The headline is separated from the data by a separator line. The column width depends on the widest value in the data.
   The title will also be taken into account.

   Output for the example above:

   Name         |Strasse         |Ort          |Alter|
   -------------+----------------+-------------+-----+
   Peter Pan    |Am Hang 5       |12345 Einsam |42   |
   Maria Schmitz|Kölner Straße 45|50123 Köln   |43   |
   Paul Meier   |Münchener Weg 1 |87654 München|65   |

*/
public class TabsCSV
{
}
