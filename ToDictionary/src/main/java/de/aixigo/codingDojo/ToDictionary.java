package de.aixigo.codingDojo;

/*
Write a function that converts a specially formatted string into a dictionary.
The following table shows some examples of strings as inputs and the resulting dictionary.

"a=1;b=2;c=3"   =>  {{"a","1"},{"b","2"},{"c","2"}}
"a=" => {{"a",""}}
"=1" => Exception
"" => {}
"a==1" => {{"a","=1"}}
 */
public class ToDictionary
{
}
