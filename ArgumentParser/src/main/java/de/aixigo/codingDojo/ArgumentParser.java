package de.aixigo.codingDojo;

//http://codingdojo.org/kata/Args/

//The arguments passed to the program consist of flags and values.
//Flags should be one character, preceded by a minus sign.
//Each flag should have zero, or one value associated with it.

//You should write a parser for this kind of arguments.
//This parser takes a schema detailing what arguments the program expects.
//The schema specifies the number and types of flags and values the program expects.


// Example 1 . -l -p 8080 -d /usr/logs -> 3 Flags l,p,d. Boolean Flags True if present
// Returns a map with flag as key and parameter as value

public class ArgumentParser
{
}
